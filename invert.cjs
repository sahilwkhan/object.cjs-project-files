module.exports = invert;

function invert(obj) {
    if (typeof obj != "object") {
        return [];
    }
    let temp;
    let invert_object = {};
    for (let index in obj) {
        invert_object[obj[index]] = index;
    }
    return invert_object;
}
