module.exports = defaults;

function defaults(obj,defaults_props) {
    if (typeof obj != "object") {
        return [];
    }
    
    for (let index in obj) {
        if (defaults_props.hasOwnProperty(index)){
            obj[index] = defaults_props[index];
        }
    }
    return obj;
}