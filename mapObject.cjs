module.exports = mapObject;


function mapObject(obj,cb) {
    if (typeof obj != "object") {
        return [];
    }
    let array_of_values = [];
    for (let index in obj) {
        obj[index] = cb(obj[index]);
        array_of_values.push(obj[index])
    }
    return array_of_values;
}
