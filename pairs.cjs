module.exports = pairs;

function pairs(obj) {
    if (typeof obj != "object") {
        return [];
    }
    let array_of_values = [];
    for (let index in obj) {
        array_of_values.push([index,obj[index]]);
    }
    return array_of_values;
}
