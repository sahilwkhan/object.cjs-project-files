module.exports = values;

function values(obj) {
    if (typeof obj != "object") {
        return [];
    }
    let array_of_values = [];
    for (let index in obj) {
        array_of_values.push(obj[index]);
    }
    return array_of_values;
}
