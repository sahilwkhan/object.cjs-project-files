module.exports = keys;

function keys(obj) {
    if (typeof obj != "object") {
        return [];
    }
    let array_of_keys = [];
    for (let index in obj) {
        array_of_keys.push(index);
    }
    return array_of_keys;
}

